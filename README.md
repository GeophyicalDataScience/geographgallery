# GeoGraphGallery

<img src="https://www.turing.ac.uk/sites/default/files/2018-05/university-edinburgh_705x209.png" align=right width=30%>

This project is a collection of exemplars for how to make geo-relevant graphs.
The content is maintained and developed by the Geophysical Data Science course and the Geophysics students in the School of GeoSciences at Edinburgh University.

- <a href="earthquakeSeismology.md">Earthquake Seismology Examples</a>
- Geophysical Exploration Examples
- <a href="meteorology.md">Meteorology Examples</a>
- Climate Examples
- Plotting on Maps

# Thumbnail Gallery Grouped by Plot Type

## Scatter Plots

<a href="Code/ScatterPlots/basicScatterPlot.ipynb"><img src="Code/ScatterPlots/basicScatterPlot.png" width=40%></a>

<a href="Code/ScatterPlots/SCaliforniaEqCatScatterPlot.ipynb"><img src="Code/ScatterPlots/SCalEqCatScatterPlot.png" width=40%></a>

## Simple Regression

## Polar Histograms

## Specialist Plots

<a href="Code/MetPlots/Skew-T.ipynb"><img src="Code/MetPlots/SkewT.png" width=40%></a>
